from controller import Controller

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

import unittest

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from webbrowser import Chrome

class Test(unittest.TestCase):
    
    def setUp(self):
        print("at setUp")
        self.driver = Controller.instantiate(self, "chrome")
        
    def test_1(self):
        print("at Test 1")
        self.driver.get("https://google.com")
        element = self.driver.find_element_by_id("lst-ib")
        
        element.send_keys("hello world")
        element.send_keys(Keys.ENTER)
        
        
    def test_2(self):
        print("at Test 2")
        self.driver.get("https://google.com") 
        
        element = WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.ID, "lst-ib")))
        element.send_keys("weather")
        element.send_keys(Keys.ENTER)
        
    def test_3(self):
        print("at Test 3")
        self.driver.get("https://amazon.com")
        element = self.driver.find_element_by_id("twotabsearchtextbox")
        
        element.send_keys("pens")
        element.send_keys(Keys.ENTER)
        
        self.driver.find_element_by_xpath("//*[@id='result_3']/div/div[3]/div[1]/a/h2").click()
        WebDriverWait(self.driver, 8).until(EC.presence_of_element_located((By.ID, 'productTitle')))
        nameoftext = self.driver.find_element_by_id('productTitle').text
        
        self.driver.find_element_by_xpath("//*[@id='quantity']").click()
        
        self.driver.find_element_by_xpath("//*[@id='quantity']/option[4]").click()
        
        self.driver.find_element_by_xpath("//*[@id='add-to-cart-button']").click()
        
        self.driver.find_element_by_xpath("//*[@id='nav-cart']/span[3]").click()
            
        assert nameoftext in self.driver.page_source
        
    def test_4(self):
        print("at Test 4")
        self.driver.get("https://amazon.com")
        
        self.driver.find_element_by_xpath("//*[@id='nav-link-shopall']")
        
        element_to_hover_over = self.driver.find_element_by_xpath("//*[@id='nav-link-shopall']")
        
        hover = ActionChains(self.driver).move_to_element(element_to_hover_over)
        
        hover.perform()
        
    def test_5(self):
        print("at Test 5")
        self.driver.get("https://pandora.com")
        
        self.driver.find_element_by_class_name("SearchField__input").click()
       
        self.driver.find_element_by_class_name("SearchField__input").send_keys("pop")
    
    def tearDown(self):
        print("at tearDown")
        self.driver.close() 
        
        
if __name__ == '__main__':
    unittest.main()