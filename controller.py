from selenium import webdriver

class Controller():
    
    def instantiate(self, browsername):
        print("at browser " + browsername)
        if browsername in "chrome" :
            self.driver = webdriver.Chrome()
        elif browsername in "firefox" :
            self.driver = webdriver.Firefox()
        elif browsername in "ie" :
            self.driver = webdriver.Ie()
            
        return self.driver
    